import 'package:flutter/material.dart';

void main() {
  runApp(Level2());
}

class Level1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Container(
        color: Colors.white,
        child: Center(
          child: Text("tarta de queso"),
        ),
      ),
    );
  }
}

class Level2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primaryColor: Colors.purple),
        home: Scaffold(
          appBar: AppBar(
            title: Text("Nombre!"),
          ),
          backgroundColor: Colors.white,
          body: Center(child: Icon(
            Icons.format_bold,
            size: 180,
            color: Colors.green)
        )));
  }
}
